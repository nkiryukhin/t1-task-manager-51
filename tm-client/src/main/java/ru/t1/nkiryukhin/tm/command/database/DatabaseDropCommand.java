package ru.t1.nkiryukhin.tm.command.database;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.dto.request.DatabaseDropRequest;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

public class DatabaseDropCommand extends AbstractDatabaseCommand {

    public static final String NAME = "database-drop";

    @NotNull
    @Override
    public String getDescription() {
        return "Drop database";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATABASE DROP]");
        System.out.println("ENTER PASSPHRASE FOR THIS OPERATION:");
        @NotNull final String passphrase = TerminalUtil.nextLine();
        @NotNull final DatabaseDropRequest request = new DatabaseDropRequest(passphrase);
        getAdminEndpoint().dropDatabase(request);
        serviceLocator.getLoggerService().info("DATABASE WAS SUCCESSFULLY DROPPED");
    }

}

package ru.t1.nkiryukhin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.dto.request.TaskCompleteByIdRequest;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Complete task by id";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(getToken());
        request.setId(id);
        getTaskEndpoint().completeTaskById(request);
    }

}

package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.DTOservice.PropertyService;
import ru.t1.nkiryukhin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.nkiryukhin.tm.api.endpoint.IUserEndpoint;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.dto.model.UserDTO;
import ru.t1.nkiryukhin.tm.dto.request.*;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.marker.IntegrationCategory;

import static ru.t1.nkiryukhin.tm.dataDTO.UserTestData.*;

@Category(IntegrationCategory.class)
public final class AuthEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private static String tokenAdmin;

    @BeforeClass
    public static void setUp() throws AbstractException {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_USER_LOGIN);
        loginRequest.setPassword(ADMIN_USER_PASSWORD);
        tokenAdmin = authEndpoint.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(tokenAdmin);
        request.setLogin(USUAL_USER_LOGIN);
        request.setPassword(USUAL_USER_PASSWORD);
        request.setEmail(USUAL_USER_EMAIL);
        userEndpoint.registryUser(request);
    }

    @AfterClass
    public static void tearDown() throws AbstractException {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(tokenAdmin);
        request.setLogin(USUAL_USER_LOGIN);
        userEndpoint.removeUser(request);
    }


    private String getUserToken() throws AbstractException {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(USUAL_USER_LOGIN);
        request.setPassword(USUAL_USER_PASSWORD);
        return authEndpoint.login(request).getToken();
    }

    @Test
    public void loginUser() throws AbstractException {
        @Nullable final String token = getUserToken();
        Assert.assertNotNull(token);
        @NotNull final UserProfileRequest request = new UserProfileRequest(token);
        Assert.assertNotNull(authEndpoint.profile(request).getUser());
    }

    @Test
    public void viewProfileUser() throws AbstractException {
        @Nullable final String token = getUserToken();
        @NotNull final UserProfileRequest request = new UserProfileRequest(token);
        @Nullable final UserDTO user = authEndpoint.profile(request).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(USUAL_USER_LOGIN, user.getLogin());
    }

    @Test
    public void logoutUser() throws AbstractException {
        @Nullable final String token = getUserToken();
        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(token);
        authEndpoint.logout(requestLogout);
        @NotNull final UserProfileRequest requestProfile = new UserProfileRequest(token);
        Assert.assertThrows(Exception.class, () -> authEndpoint.profile(requestProfile));
    }

}

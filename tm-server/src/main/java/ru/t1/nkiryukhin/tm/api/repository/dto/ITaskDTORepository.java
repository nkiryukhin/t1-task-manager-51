package ru.t1.nkiryukhin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDTORepository extends IUserOwnedDTORepository<TaskDTO> {

    @Nullable
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
package ru.t1.nkiryukhin.tm.service;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.IDatabaseProperty;
import ru.t1.nkiryukhin.tm.dto.model.ProjectDTO;
import ru.t1.nkiryukhin.tm.dto.model.SessionDTO;
import ru.t1.nkiryukhin.tm.dto.model.TaskDTO;
import ru.t1.nkiryukhin.tm.dto.model.UserDTO;
import ru.t1.nkiryukhin.tm.model.Project;
import ru.t1.nkiryukhin.tm.model.Session;
import ru.t1.nkiryukhin.tm.model.Task;
import ru.t1.nkiryukhin.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperty;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.entityManagerFactory = buildEntityManagerFactory();
    }

    @NotNull
    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    @NotNull
    @Override
    @SneakyThrows
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }


    @Override
    public void close() {
        entityManagerFactory.close();
    }

    @NotNull
    @SneakyThrows
    private EntityManagerFactory buildEntityManagerFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(Environment.URL, databaseProperty.getDatabaseUrl());
        settings.put(Environment.USER, databaseProperty.getDatabaseUser());
        settings.put(Environment.PASS, databaseProperty.getDatabasePassword());
        settings.put(Environment.DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, databaseProperty.getDatabaseShowSQL());

        settings.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getDatabaseUseSecondLevelCache());
        settings.put(Environment.USE_QUERY_CACHE, databaseProperty.getDatabaseUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, databaseProperty.getDatabaseUseMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, databaseProperty.getDatabaseCacheRegionPrefix());
        settings.put(Environment.CACHE_REGION_FACTORY, databaseProperty.getDatabaseCacheRegionFactoryClass());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getDatabaseCacheProviderConfigurationFile());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    @Override
    public Liquibase getLiquibase() throws IOException, SQLException, DatabaseException {
        final ClassLoaderResourceAccessor accessor = new ClassLoaderResourceAccessor();
        final Properties properties = new Properties();
        final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        final Connection connection = DriverManager.getConnection(properties.getProperty("url"),
                properties.getProperty("username"), properties.getProperty("password"));
        final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        final Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
        final String changeLogFile = properties.getProperty("changeLogFile");
        return new Liquibase(changeLogFile, accessor, database);
    }

}